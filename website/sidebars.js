module.exports = {
  docs: [
    {
      type: 'doc',
      id: 'what-is-it',
    },
    {
      type: 'doc',
      id: 'crop-rotation',
    },
    {
      type: 'doc',
      id: 'stopped-result',
    },
    {
      type: 'category',
      label: 'versioning',
      items: [
        'versions/v16',
        'versions/v15',
        'versions/v14',
        'versions/v13',
        'versions/v12',
        'versions/v11',
        'versions/v10',
        'versions/v9',
        'versions/v8',
        'versions/v7',
        'versions/v6',
        'versions/v5',
        'versions/v4',
        'versions/v3',
        'versions/v2',
        'versions/v1',
      ],
    },
    {
      type: 'category',
      label: 'notes',
      items: [
        'notes/dharma-wheel',
        'notes/why-ganesha',
        'notes/suffering',
        'notes/key-terms',
        'notes/whimpering-vibes',
        'notes/eating-meat',
        'notes/daily-notes',
        'notes/mindmaps'
      ],
    },
    {
      type: 'category',
      label: 'illustrations',
      items: [
        'illustration_doc/cover_ev',
        'illustration_doc/tooling',
        'illustration_doc/plants',
        'illustration_doc/bodies'
      ],
    },
  ],
};
