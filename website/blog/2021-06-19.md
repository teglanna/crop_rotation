---
slug: june-19
title: survive in 40 degree
author: teglanna
author_title: gardener
author_url: https://gitlab.com/teglanna
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/1856130/avatar.png?width=400
tags: [doing, lichthof, saving plants]
---

## This is the first day that I started to get rid of worries

* Even though it is too much hot and the conditions are not good for growing plants.
* But that was the case.
* Don't forget Anna!
* Investigating their resilience and your persistence.

![](../static/img/21.jpg)

<!--truncate-->

That's happening.

I'm at home and dirty and intent, I'm doing it anyway.

They have been growing for almost a week.

This is the case, the "fixing situation."
I plant them too much early, but I wanted to try out this ten-days-setup.

**So here, you can see the ad hoc solutions:**

![](../static/img/22.jpg)
![](../static/img/23.jpg)
![](../static/img/23b.jpg)
![](../static/img/24.jpg)
![](../static/img/25.jpg)
![](../static/img/26.jpg)
![](../static/img/27.jpg)
![](../static/img/28.jpg)


