---
id: v6
title: Version 6
sidebar_label: v6
---

## REMOVED (a lot)

(even though it contained funny-interesting threads)

### Experiencing, whimpering

[Where to put my struggling? put it or not or hide it?](../notes/whimpering-vibes)

### Hmm, an apple

Because you are in the garden, even it is strange, you find nice fruits and vegetables around you. As you are studying the nice, healthy apple tree, you suddenly notice a nice red piece on the ground. You lift it up, and take a bite of it. It doesn’t have any taste and you don’t feel, that something had gone into your body.

Look delicious. Now you can take your time, and just try it out, how it tastes.
Than do you fill your body “correctly”?

```well-containment```

Is it possible to do it in a more efficient way? What about self-containment 3 ?
What does it mean being well-contained?

3 *“...skin-as-container reinforces feelings of aliveness and existence, whereas the lack of containment fosters a state
of incoherence associated with anxiety and annihilation...” (Manning, 2013)*

If you don’t catch on the hop, what this apple “does” with your body, than what is happening in your body without the apple?

What does it contain? If you contain your hands, you could probably let the rake away earlier. Or is it about focusing? Does it happens then in your brain? In your mind? *A looking becomes a touching, a feeling becomes a hearing.*

## ADDED

### unfunny title (but important remark)

Suffering well - practical hints

If you have a big farm with garden and animals to take care, you don’t have time to feel depressive. This is an extreme example, but the solution to learn, how to “enjoy uncomfortable situation” is in the acceptance and humility of the events.

**preperception:**

James, says, in the interpretation of Shusterman:
*“To conquer unwanted emotions (such as depression or sullenness or fear), we should “cold-bloodedly. . . go through the outward movements of those contrary dispositions which we prefer to cultivate” for “by regulating the action, which is under the more direct control of the will, we can indirectly regulate the feeling, which is not.” Thus to attain or regain cheerfulness, we should
simply “act and speak as if cheerfulness were already there.”*

In my reading it means, that you experience the situation in its totality with every feelings popped up, every thoughts, however you don’t go into those feelings, you don’t identify yourself with those.

**You can stay in an outsider position.**

### Somatic introspection part inspired by W. James, and I'm very grateful for Shusterman too, because in his reading I could get closer to the main concept

```Take and keep interest on body functions.```

Taking interest means: you are sick, your
body acts as you don’t expect, and don’t
understand.
You desire much more energy, you don’t
understand, the reason you don’t have.
There are several studies about the
internal tract and the brain functioning
relation, people try newer and newer diets,
but the simplest way to get an
understanding is just right there. Pay
attention their own body.
So this is a big motivation to get the
interest.
Keeping the interest means, that you are
able, and motivated to experiment your
own body. To see how it changes. Several
times there are just nuance changes, so it needs a slower tempo and a try to express the tinted
feelings.
To get this detailed and close view, you have to pay attention.

```The difficulty: nehéz figyelni a szívverésre pölö - **hard to focus on heartbeat**```

Feelings of “the beating of our hearts and arteries, our breathing, [and even] certain steadfast bodily pains” are hard to focus on since they tend to fade into the stable felt background that frames our conscious focus, and that focus tends anyway to concentrate not on the discrimination of bodily feelings but on the discrimination of external things.

```tulzott odafigyeles sem tesz jt: (431.o) – ez egy fontos resz!
but too much focus is unhealthy as well```

Figyeled a kezed vagy nem?
mi a praktikus, amikor éppen cselekedni kell és te a kezedre figyelsz.
Viszont biciklizés, új készség megtanulása, begyakorlása → később spontán akt lesz.

“Once it is established, then focused attention to the bodily means and sensations of swinging can be relinquished to sink back into the unattended background...” (Shman, somatic, 431.o)
Shusterman: váratlan szituban viszont milyen jó, hogy képes vagy azonnal reagálni. (431.o)
igazából ez a rész az érdekes itt. Kant és James egy követ fújnak.
James argues, that too much introspection is not good,

“This inner view and self-feeling weakens the body and diverts it from animal functions...” 9