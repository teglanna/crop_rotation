---
id: crop-rotation
title: Introduction
sidebar_label: crop rotation
---

### Naming the phenomenon

**Crop rotation** is the practice of growing a series of different types of crops in the same area across a sequence of growing seasons. It reduces reliance on one set of nutrients, pest and weed pressure, and the probability of developing resistant pest and weeds.

### Text structure

```This is an arbitrary manual is based on a parallel was drawn between gardening and mind processes.```

Garden as a scene provides a good, friendly, safe but also a condensed location for learning by doing and can reach wider audience thanks to that gardening covers a wide range of levels between being a professional or a simple field-digging nature-lover.


It is based on an excursion was taken in the end of the summer, in the beginning of
harvesting season. The first location is the garden, where the reader meets with the ever-changing nature, and learn how to use some garden instruments. The second chapter takes place in the house, where the user is driven to an inside and body-side gardening, to get an understanding of how to approach his/her own body. The third part and its scenery happens even more inside, on the mind level to get a picture, where and how the collected experiences are stored and offers a practice to train the brain and weed out certain thoughts when it is necessary...

![](img/keep_the_tempo.png)
