---
id: suffering
title: And clinging, and being attached, and letting go...
sidebar_label: more on suffering
---

[Video](http://annateglassy.great-site.net/suffering-and-changing/) to understand grabbing --> in order to understand letting go
--> to understand being attached...

After introducing the main issue and an Indian “superstar” it is quite difficult to continue, because now I should immediately jump into the practical part, which answers the question:

* “How to learn that flexible attitude?” (and what to learn exactly)
* How to process experiences?
* How to digest and store those in a conscious way?

Returning back to suffering and attachment. In a very raw and simple explanation I would like to introduce the Buddhist wheel of life. (I don’t want to seem pedantic, but I can’t pass by those teachings, when those are clear and detailed enough and very handy in the daily life.)

When you look carefully the links, and how those follow each other, you will see the Achilles heel of the cycle. It is about **grasping.** Clinging and grasping, being attached by something or someone. Also after looking at the “original” representations several times, I have discovered, that this process is *all about experiencing.*

It seems, that the links one by one describes the normal human behavior, we can’t really avoid, but there is the point where **you can break the chain,** and it is when you can be aware of your attachments and you are able to let them go, when it is necessary.

For understanding more that instability and variability is very handy to observe surrounding nature and its cycles, therefore I decided to connect it with a new experience gained in the garden and I noted down what I consciously leared.