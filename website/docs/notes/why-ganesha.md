---
id: why-ganesha
title: Referring to Ganesha again and again
sidebar_label: why Ganesha
---

**The most well-known story about his birth** tells that Parvati created a man from clay to protect her until she was taking bath. When Shiva, her husband arrived back, Ganesha didn’t recognized his father and he didn’t let him approach Parvati, therefore Shiva cut his head. When he got known about his newly born son, he replaced Ganesha’s head with the animal, which arrived first. (It was
fortunately an elephant, which is famous its wisdom and good memory.) Ganesha, who lost his head, had to get a new one immediately to survive, and he had to adapt to the suddenly happened tragedy. Besides he inherited all of his fathers knowledge too, who is known of his destructive nature.

Despite of destroying an unsustainable situation, which requires a new attitude, he is also the protector of the new adventures, devotees believe that if Ganesha is propitiated, he grants success,prosperity and protection against adversity.
The other “cool story” about him is, that he wrote down Mahabharata.

/*Ganesha is the Lord of Obstacles, both of a material and spiritual order. He is popularly worshipped as a remover of obstacles, though traditionally he also places obstacles in the path of those who need to be checked. Hence, he is
often worshipped by the people before they begin anything new... he is considered to be the Lord of letters and learning.He wanted to write the story was recited by Vyasa uninterruptedly, therefore he has to listen, understand and note at the same time. It resulted that he was in a “hurry” and his pen broke at one point, but he just cut off one of his tusk, and the transcription could proceed without interruption, permitting him to keep his word and complete his task.*/

![](../img/ganesha.png)
