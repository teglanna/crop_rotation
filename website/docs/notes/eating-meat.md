---
id: eating-meat
title: Meeting eat
sidebar_label: eating meat
---


* obvious, common, habitual, "affects" everyone, ritualistic(?)
* takes too much time: physically, mentally
* increase clinging(?!) - but what is appetite and hunger? (the smaller is tastier...)
* outdated addictions

-> control, pay attention eating: less thoughts around it

Eating (the animal) (and eating at all…) Investigation, action and result → dissonance, what about learning and experiencing? Body observation with food (dropping food to the machine) Beledobom a gépbe, mit ad ki. Taking the first step inside (out)

    Eating meat, meeting eat “The very strange thing is that only a few years later, I, too, found the pressures of daily life in American society so strong that I gave up on my once-passionate vegetarianism, and for a while all my intense ruminations went totally underground. I think that the me of the mid-sixties would have found this reversal totally unfathomable, and yet the two versions of me had both lived in the very same skull. Was I really the same person?”[Hofstadter]

Unwanted longings. The main question is not about flesh it is about eating at all. The relation between me and the food. What do I want to “gain” from food? To understand my desire and wish, I have to talk to my guts and to my stomach.

How much important is eating meat? What is the difference between meat and flesh? Hofstadter admit about his attitude of eating meat and being vegetarian. Thinking about eating (meat) took too much energy and attention from my life. So I had to investigate it and understand this longing. But also understand how eating can become to an addiction. Here I’m just talking about my struggling with the right decision which is proper for my beliefs. According to my “previous” belief, that my blood type and lack of iron and vitamin B12 I had to consume flesh regularly. I couldn’t really investigate the origin of that longing until I tried to communicate to my guts. To reach that signal I had to try not to eat for a day. It wasn’t that much difficult as I predicted.

After learning the signs of my digestive tract I gained a view of meat. I have found the next points: • meat eating gives power • that power is rawer than the other • meat eating creates addiction with eating at all • it increases the hunger (in every sense) • it increases animal instinct • it mixes my feelings

Eating less, no meat, just few, in the morning: • too much fire • increased activity, has to be controlled

Eating as an addiction A ritual to have and prepare pretty meal. Without cheese, meat, and many different ingredients, with less taste it is easier to focus on the target of the day.
