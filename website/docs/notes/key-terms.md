---
id: key-terms
title: The abstract level
sidebar_label: key terms
---

#### ca_anx

**Cartesian anxiety** refers to that notion and comes naturally from that dualism:
people has suffered from a longing for ontological certainty. It includes the desire that the surrounding world should be able to handle as a separated entity from ourselves, therefore we can have an unchanging knowledge
about ourselves and itself.


#### co_di

**The annoying part of learning route**

When we meet with something new, which doesn’t fit into our worldview, however it “still works” in the opposite way, we become insecure, and loose our balance, because we have to choose between the two contradictory beliefs or values. Those two views are “living” in us at the same time and we are going to change unintentionally, because we have learned something new. This is called: **cognitive dissonance.**

#### ne_cap

John Keats says, Shakespeare is one of the people, who are not even adapt to the challenges but seek those situations, even though it creates states, they can’t prepare before. It is a brave artistic attitude, face the unknown just to learn and create something new. This ability is called **negative capability.**