---
id: what-is-it
title: Getting Started
sidebar_label: what is it?
slug: /
---

```This is a documentation site for structuring a masters diploma.```

Originally it was created to keep track the core changes of the written part, but under **Lichthof_B_log** you can find the daily notes during the preparation and the setup of the artwork.

In **FAQ** section you can find the questions arise more often during the process. Those are usually the triggering ones for thinking toward.


## Step 1: take an overview

* [Crop rotation](crop-rotation) is the title of the text, gives a short introduction.
* [Final state](stopped-result) contains a general remark on the result and a link to the page with a shorter version to have a taste.
* Under [versioning](versions/v16) section you will find the determinative changes. However, it is organized in an arbitrary way, I believe, it can represent truly the main differences between the phases.
* [Notes](notes/dharma-wheel) holds the writings and concepts which didn't fit in the framework of the text, but come up with other interesting approaches.
* [Illustrations](illustration_doc/cover_ev) is a set of my drawing are used in the different variants.


## Step 2: start investigating

* I recommend to visit illustrations, maybe it can be motivating...
* ...or try the button in the top right corner. It is "provided" by Docusaurus, which I can recommend wholeheartedly.
* Also by clicking different versions randomly can be funny.
* ...or you can follow the "built in" arrows below on the pages, but i think it is just boring.

## That's it!

Congratulations! You've successfully reached the end of the description.
