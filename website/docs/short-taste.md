---
id: stopped-result
title: Get a taste
sidebar_label: final state
---


```Am I satisfied with version_16? No, not really, maybe around 75%. The rest was written in the last weeks, and it would need time to incubate. Interesting question, why do I stop it here though? No, I'm not talking about time constrain, it is more about the changes of circumstances, and that I'm heading now somewhere else, I'm developing this website to understand, what happened during the last six months with this sentences and pictures. And of course, it gives me a good reason to write about...```

![](img/thesis/thesis_folder.jpg)

## Content


- To the Reader........................................................................................................................................................
- Preface..................................................................................................................................................................
- Before going out – preparation...........................................................................................................................
   - Season forecast...............................................................................................................................................
   - Proper wear....................................................................................................................................................
- Grab the rake – man in the garden......................................................................................................................
   - Rake the straw................................................................................................................................................
   - Clean the neighbor’s vineyard........................................................................................................................
   - Take out potatoes............................................................................................................................................
   - Get deeper into the ground.............................................................................................................................
   - Take out red beet............................................................................................................................................
   - +1 Cornus mas – “húsos som”.......................................................................................................................
- Let the rake – someone in your body..................................................................................................................
   - Do Nothing.....................................................................................................................................................
   - Back to the jungle – random bypass..............................................................................................................
   - Get back online..............................................................................................................................................
   - Fill to feel.......................................................................................................................................................
- Grab your thoughts – someone in your mind.....................................................................................................
   - Rake your mind - Visceral doings..................................................................................................................
      - 0 Prepare your body-tool..........................................................................................................................
      - 1. Mark the edges......................................................................................................................................
      - 2. Make it concrete....................................................................................................................................
      - 3. Create pain channels – “anger in the liver”...........................................................................................
      - 4. Do the “naming” and the final act.........................................................................................................
      - 5. The result: pretty brain & good mood...................................................................................................
- Closing remarks..................................................................................................................................................
- Bibliography.......................................................................................................................................................


## To the Reader

This manual is written based on a parallel that was drawn between gardening and mind processes. It explains
how to rewire your brain with the help of gardening job which is reshaped season by season. It touches areas of
the route that can be taken from thinking to materialization, including emotional and visceral levels.

It addresses people, who like gardening. Garden as a scene provides a good, friendly, safe but also a condensed
location for learning by doing and can reach wider audience due to the fact that gardening covers a wide range of
levels between being a professional and a simple field-digging nature-lover.

If you are one of those people, I hope you will find it useful during your daily routine.

It can be used as a tool in several ways, without any restrictions, however the chapters are built upon each other.
Don’t take it too seriously, and please read it with an open heart and take “warning” statements only if the cap
fits...

![](img/thesis/before.jpg)


## Season forecast

**Crop rotation** is the practice of growing a series of different types of crops in the same area across a sequence of
growing seasons. It reduces reliance on one set of nutrients, pest and weed pressure, and the probability of
developing resistant pest and weeds.

**Harvesting** is the process of gathering a ripe crop from the fields, it is a phase of crop rotation.
Harvesting and ingathering include the following tasks: picking fruits, vegetables, and post harvesting: handling

- sorting, cleaning, packing and cooling.
Storing the food is followed by cleaning up the carrying medium, the ground, to prepare it for the silent period. It
includes reworking, rotating the soil and planting covering grains, which preserve the field’s condition for the
next season by protecting the nutrients in it.

In the old rural traditions the completion of harvesting is celebrated with ceremony or festival, which has social
and cultural importance, people gather together and enjoy the fruits of their previous effort, as it marks the end of
a particular crop. Living there, considering the natural cycles is obvious and draws the daily routine at all time.

This lifestyle, in harmony with nature, truly represents a **just-do-it** life.
There are no questions and time to hesitate.

![](img/thesis/grab.jpg)


```
Gardening Iconography
```

_“Individuals need tools to move and to dwell. They need remedies for their diseases and means to communicate
with one another. People cannot make all these things for themselves. They depend on being supplied with
objects and services which vary from culture to culture. Some people depend on the supply of food and others on
the supply of ball bearings.”
(_ Illich, 1985, p. 12)

Here are some gardening tools to “ **touch** ” and “ **move** ” soil, according to what the given situation requires.

Grabbing the rake means that you already know, how to catch and hold it.

It is not a big deal. You have to collect power to hold it.

Where does the rake-grabbing power come from?

How strong is your grip at all? How firmly are you able to grip?

First remark before going ahead:

**“More brain than brawn.”** (Most of the time.)


## Rake the straw

_“Keep your eye on the place aimed at, and your hand will fetch it.”_^4

- The wooden rake is a possible extension of the arms.
- Raking hay, which is actually straw means collecting the dried and cropped grass and weed from the
    meadow. It is called hay, when it contains dried grain stalk instead of grass.
- It can be used as food for the cattle or as covering material at wintertime.
- This is the end of the story with its “dried result” everywhere has to be collected.
- It can happen at the end of August or in September during a few sunny days, before the rain comes.
- The days are counted, and the best time for raking is the hottest at the same time.
- The biggest effort is needed to complete the mission.
- Better to eat less and drink more during the day. Red beet from the garden provides superhuman physical
    power, or maybe some meat can help as well... (That’s another interesting question.)
- The “how to do it” shapes its form after some days, it is more about the technique, what matters.


![](img/thesis/border.png)

[There is a much fancier website (which is quite slow unfortunately for first loading (free host)) offering a view of the final version.](http://annateglassy.great-site.net/crop-rotation/?i=1)

