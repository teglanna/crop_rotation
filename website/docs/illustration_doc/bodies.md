---
id: bodies
title: Bodily states
sidebar_label: bodily
---

![](../img/grab_thought.png)
![](../img/grab_thought2.png)
![](../img/injuries.png)
![](../img/blood_letting.png)
![](../img/pain_channel.png)
![](../img/pretty_brain.png)
![](../img/vitality_level.png)
![](../img/this_is_me.png)
![](../img/this_is_me2.png)
![](../img/hand_right_icon.png)
![](../img/trepanation.png)
![](../img/hand_up_icon2.png)
![](../img/layer_guy2.png)

