/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'My Site',
  tagline: 'The tagline of my site',
  url: 'https://teglanna.gitlab.io',
  baseUrl: '/crop_rotation/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/cp_fav.png',
  organizationName: 'GitLab', // Usually your GitHub org/user name.
  projectName: 'crop_rotation', // Usually your repo name.
  themeConfig: {
    navbar: {
      logo: {
        alt: 'crop rotation logo',
        src: 'img/cp_fav.png',
      },
      items: [
        {
          to: '/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
          {to: 'blog',
          label: 'Lichthof_B_log',
          position: 'left'
        },
        {to: 'question',
          label: 'FAQ',
          position: 'right'
        },
      ],
    },
    footer: {
      style: 'dark',
      copyright: `Copyright © ${new Date().getFullYear()} teglanna, built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          //path: './docs',
          routeBasePath: '/', // Set this value to '/'.
          //homePageId: 'what-is-it', // Set to existing document id.
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
